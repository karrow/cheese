# translation of cheese to Turkish
# Copyright (C) 2008, 2009, 2011 the Free Software Foundation, Inc.
# This file is distributed under the same license as the cheese package.
#
# Baris Cicek <baris@teamforce.name.tr>, 2008, 2009, 2011.
# Yaşar Şentürk <yasarix@gmail.com>, 2014.
# Muhammet Kara <muhammetk@gmail.com>, 2011, 2012, 2013, 2014, 2015, 2016.
# Furkan Ahmet Kara <furkanahmetkara.fk@gmail.com>, 2017.
# Emin Tufan Çetin <etcetin@gmail.com>, 2017, 2020.
# Çağatay Yiğit Şahin <cyigitsahin@outlook.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: cheese\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/cheese/issues\n"
"POT-Creation-Date: 2020-06-12 08:06+0000\n"
"PO-Revision-Date: 2019-08-08 17:12+0300\n"
"Last-Translator: Çağatay Yiğit Şahin <cyigitsahin@outlook.com>\n"
"Language-Team: Turkish <gnome-turk@gnome.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Gtranslator 3.32.1\n"

#: data/cheese-main-window.ui:52
msgid "Photo mode"
msgstr "Fotoğraf kipi"

#: data/cheese-main-window.ui:53
msgid "Photo"
msgstr "Fotoğraf"

#: data/cheese-main-window.ui:65
msgid "Video mode"
msgstr "Video kipi"

#: data/cheese-main-window.ui:66
msgid "Video"
msgstr "Video"

#: data/cheese-main-window.ui:77
msgid "Photo burst mode"
msgstr "Şipşak kipi"

#: data/cheese-main-window.ui:78
msgid "Burst"
msgstr "Şipşak"

#: data/cheese-main-window.ui:107 src/cheese-window.vala:1415
msgid "Take a photo using a webcam"
msgstr "Web kamerası kullanarak fotoğraf çek"

#: data/cheese-main-window.ui:133
msgid "Navigate to the previous page of effects"
msgstr "Önceki etki sayfasına git"

#: data/cheese-main-window.ui:148
msgid "Effects"
msgstr "Etkiler"

#: data/cheese-main-window.ui:157
msgid "_Effects"
msgstr "_Etkiler"

#: data/cheese-main-window.ui:170
msgid "Navigate to the next page of effects"
msgstr "Sonraki etki sayfasına git"

#: data/cheese-main-window.ui:192
msgid "Leave fullscreen mode and go back to windowed mode"
msgstr "Tam ekran kipinden çık ve pencereli kipe geri dön"

#: data/cheese-prefs.ui:7
msgid "Preferences"
msgstr "Tercihler"

#: data/cheese-prefs.ui:13 data/headerbar.ui:21
msgid "_Help"
msgstr "_Yardım"

#: data/cheese-prefs.ui:22
msgid "_Close"
msgstr "_Kapat"

#: data/cheese-prefs.ui:51
msgid "Device"
msgstr "Aygıt"

#: data/cheese-prefs.ui:66
msgid "Photo resolution"
msgstr "Fotoğraf çözünürlüğü"

#: data/cheese-prefs.ui:81
msgid "Video resolution"
msgstr "Video çözünürlüğü"

#: data/cheese-prefs.ui:140 libcheese/cheese-fileutil.c:283
#: libcheese/cheese-fileutil.c:303
msgid "Webcam"
msgstr "Web kamerası"

#: data/cheese-prefs.ui:159
msgid "Brightness"
msgstr "Parlaklık"

#: data/cheese-prefs.ui:174
msgid "Saturation"
msgstr "Doygunluk"

#: data/cheese-prefs.ui:189
msgid "Hue"
msgstr "Ton"

#: data/cheese-prefs.ui:280
msgid "Contrast"
msgstr "Zıtlık"

#: data/cheese-prefs.ui:298
msgid "Image"
msgstr "Görüntü"

#: data/cheese-prefs.ui:324
msgid "Shutter"
msgstr "Deklanşör"

#: data/cheese-prefs.ui:338
msgid "_Countdown"
msgstr "_Geri sayım"

#: data/cheese-prefs.ui:358
msgid "Fire _flash"
msgstr "_Flaş kullan"

#: data/cheese-prefs.ui:394
msgid "Burst mode"
msgstr "Şipşak kipi"

#: data/cheese-prefs.ui:412
msgid "Number of photos"
msgstr "Fotoğraf sayısı"

#: data/cheese-prefs.ui:427
msgid "Delay between photos (seconds)"
msgstr "Fotoğraflar arasındaki bekleme (saniye)"

#: data/cheese-prefs.ui:486
msgid "Capture"
msgstr "Yakala"

#: data/headerbar.ui:6
msgid "_Fullscreen"
msgstr "_Tam Ekran"

#: data/headerbar.ui:10
msgid "P_references"
msgstr "Te_rcihler"

#: data/headerbar.ui:16
msgid "_Keyboard Shortcuts"
msgstr "_Klavye Kısayolları"

#: data/headerbar.ui:25
msgid "_About Cheese"
msgstr "Peynir _Hakkında"

#: data/headerbar.ui:32 libcheese/cheese-avatar-chooser.c:85
#: src/cheese-window.vala:1444
msgid "Take a Photo"
msgstr "Fotoğraf Çek"

#: data/menus.ui:6
msgid "Open"
msgstr "Aç"

#: data/menus.ui:11
msgid "Save _As…"
msgstr "Farklı _Kaydet..."

#: data/menus.ui:16
msgid "Move to _Trash"
msgstr "Çöp’e _Taşı"

#: data/menus.ui:21
msgid "Delete"
msgstr "Sil"

#. Both taken from the desktop file.
#: data/org.gnome.Cheese.appdata.xml.in:9 data/org.gnome.Cheese.desktop.in:3
#: src/cheese-application.vala:116 src/cheese-application.vala:574
msgid "Cheese"
msgstr "Peynir"

#: data/org.gnome.Cheese.appdata.xml.in:10 data/org.gnome.Cheese.desktop.in:5
#: src/cheese-application.vala:569
msgid "Take photos and videos with your webcam, with fun graphical effects"
msgstr ""
"Web kameranızdan fotoğraflar ve videolar çekip bunlara görsel etkiler ekler."

#: data/org.gnome.Cheese.appdata.xml.in:12
msgid ""
"Cheese uses your webcam to take photos and videos, applies fancy special "
"effects and lets you share the fun with others."
msgstr ""
"Peynir; web kameranızı kullanarak fotoğraf ve video çeker, hoş etkiler "
"uygular ve eğlenceyi başkalarıyla paylaşmanıza olanak tanır."

#: data/org.gnome.Cheese.appdata.xml.in:16
msgid ""
"Take multiple photos in quick succession with burst mode. Use the countdown "
"to give yourself time to strike a pose, and wait for the flash!"
msgstr ""
"Şipşak kipinde hızlıca birçok fotoğraf çekin. Poz verebilmek için "
"sayacı kullanarak kendinize zaman tanıyın ve flaşı bekleyin!"

#: data/org.gnome.Cheese.appdata.xml.in:21
msgid ""
"Under the hood, Cheese uses GStreamer to apply fancy effects to photos and "
"videos. With Cheese it is easy to take photos of you, your friends, pets or "
"whatever you want and share them with others."
msgstr ""
"Peynir, fotoğraflarınıza ve videolarınıza hoş etkiler vermek için perde "
"arkasında GStreamer'i kullanır. Peynir ile kendinizin, arkadaşlarınızın, "
"hayvanlarınızın ve istediğiniz her şeyin fotoğrafını çekmek ve başkalarıyla "
"paylaşmak çok kolay."

#: data/org.gnome.Cheese.desktop.in:4
msgid "Webcam Booth"
msgstr "Web Kamerası Standı"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Cheese.desktop.in:7
msgid "photo;video;webcam;camera;selfie;"
msgstr "fotoğraf;video;görüntü;web kamerası;webcam;kamera;özçekim;selfie"

#: data/org.gnome.Cheese.gschema.xml:5
msgid "Use a countdown"
msgstr "Geri sayım kullan"

#: data/org.gnome.Cheese.gschema.xml:6
msgid "Set to true to show a countdown before taking a photo"
msgstr ""
"Fotoğraf çekmeden önce geri sayım göstermesi için doğru (true) olarak "
"ayarlayın"

#: data/org.gnome.Cheese.gschema.xml:11
msgid "Countdown length"
msgstr "Geri sayım uzunluğu"

#: data/org.gnome.Cheese.gschema.xml:12
msgid "The duration of the countdown before taking a photo, in seconds"
msgstr "Fotoğraf çekmeden önceki geri sayım süresi, saniye türünden"

#: data/org.gnome.Cheese.gschema.xml:18
msgid "Fire flash before taking a photo"
msgstr "Resim çekmeden önce flaş patlat"

#: data/org.gnome.Cheese.gschema.xml:19
msgid "Set to true to fire a flash before taking a photo"
msgstr ""
"Resim çekmeden önce flaş patlatılması için doğru (true) olarak ayarlayın"

#: data/org.gnome.Cheese.gschema.xml:24
msgid "Camera device string indicator"
msgstr "Kamera aygıt dizgesi belirteci"

#: data/org.gnome.Cheese.gschema.xml:25
msgid ""
"The path to the device node which points to the camera, for example /dev/"
"video0"
msgstr "Kamerayı belirten aygıt düğümünün yolu, örneğin /dev/video0"

#: data/org.gnome.Cheese.gschema.xml:30
msgid "Last selected effect"
msgstr "Son seçilen etki"

#: data/org.gnome.Cheese.gschema.xml:31
msgid "Name of the installed effect that was selected last"
msgstr "Son seçilen kurulu etkinin adı"

#: data/org.gnome.Cheese.gschema.xml:36
msgid "Photo width"
msgstr "Fotoğraf genişliği"

#: data/org.gnome.Cheese.gschema.xml:37
msgid "The width of the image captured from the camera, in pixels"
msgstr "Kameradan yakalanan resmin genişliği, piksel türünden"

#: data/org.gnome.Cheese.gschema.xml:43
msgid "Photo height"
msgstr "Fotoğraf yüksekliği"

#: data/org.gnome.Cheese.gschema.xml:44
msgid "The height of the image captured from the camera, in pixels"
msgstr "Kameradan yakalanan resmin yüksekliği, piksel türünden"

#: data/org.gnome.Cheese.gschema.xml:50
msgid "Video width"
msgstr "Video genişliği"

#: data/org.gnome.Cheese.gschema.xml:51
msgid "The width of the video captured from the camera, in pixels"
msgstr "Kameradan yakalanan videonun genişliği, piksel türünden"

#: data/org.gnome.Cheese.gschema.xml:57
msgid "Video height"
msgstr "Video yüksekliği"

#: data/org.gnome.Cheese.gschema.xml:58
msgid "The height of the video captured from the camera, in pixels"
msgstr "Kameradan yakalanan videonun yüksekliği, piksel türünden"

#: data/org.gnome.Cheese.gschema.xml:64
msgid "Image brightness"
msgstr "Resim parlaklığı"

#: data/org.gnome.Cheese.gschema.xml:65
msgid "Adjusts the brightness of the image coming from the camera"
msgstr "Kameradan gelen resmin parlaklığını ayarlar"

#: data/org.gnome.Cheese.gschema.xml:71
msgid "Image contrast"
msgstr "Resim zıtlığı"

#: data/org.gnome.Cheese.gschema.xml:72
msgid "Adjusts the contrast of the image coming from the camera"
msgstr "Kameradan gelen resmin karşıtlığını ayarlar"

#: data/org.gnome.Cheese.gschema.xml:78
msgid "Image saturation"
msgstr "Resim dolgunluğu"

#: data/org.gnome.Cheese.gschema.xml:79
msgid "Adjusts the saturation of the image coming from the camera"
msgstr "Kameradan gelen resmin doygunluğunu ayarlar"

#: data/org.gnome.Cheese.gschema.xml:85
msgid "Image hue"
msgstr "Görüntü tonu"

#: data/org.gnome.Cheese.gschema.xml:86
msgid "Adjusts the hue (color tint) of the image coming from the camera"
msgstr "Kameradan gelen görüntünün renk tonunu ayarlar"

#: data/org.gnome.Cheese.gschema.xml:92
msgid "Video path"
msgstr "Video yolu"

#: data/org.gnome.Cheese.gschema.xml:93
msgid ""
"Defines the path where the videos are stored. If empty, “XDG_VIDEOS_DIR/"
"Webcam” will be used."
msgstr ""
"Videoların saklandığı dosya yolunu belirtir. Eğer boşsa, “XDG_VIDEOS_DIR/"
"Webcam” kullanılacak."

#: data/org.gnome.Cheese.gschema.xml:98
msgid "Photo path"
msgstr "Fotoğraf yolu"

#: data/org.gnome.Cheese.gschema.xml:99
msgid ""
"Defines the path where the photos are stored. If empty, “XDG_PICTURES_DIR/"
"Webcam” will be used."
msgstr ""
"Fotoğrafların saklandığı dosya yolunu belirtir. Eğer boşsa, "
"“XDG_PICTURES_DIR/Webcam” kullanılacak."

#: data/org.gnome.Cheese.gschema.xml:104
msgid "Time between photos in burst mode"
msgstr "Patlama kipinde fotoğraflar arasındaki süre"

#: data/org.gnome.Cheese.gschema.xml:105
msgid ""
"The length of time, in milliseconds, to delay between taking each photo in a "
"burst sequence of photos. If the burst delay is less than the countdown "
"duration, the countdown duration will be used instead."
msgstr ""
"Fotoğraf patlama sırasında her fotoğraf arasında geçecek zamanın milisaniye "
"türünden uzunluğu. Eğer flaş gecikmesi geri sayım değerinden düşükse, bunun "
"yerine geri sayım değeri kullanılır."

#: data/org.gnome.Cheese.gschema.xml:111
msgid "Number of photos in burst mode"
msgstr "Patlama kipinde fotoğraf sayısı"

#: data/org.gnome.Cheese.gschema.xml:112
msgid "The number of photos to take in a single burst."
msgstr "Tek patlamada çekilecek fotoğraf sayısı."

#: data/shortcuts.ui:12
msgctxt "shortcut window"
msgid "Overview"
msgstr "Genel Bakış"

#: data/shortcuts.ui:16
msgctxt "shortcut window"
msgid "Fullscreen on / off"
msgstr "Tam ekran aç / kapa"

#: data/shortcuts.ui:22
msgctxt "shortcut window"
msgid "Quit the application"
msgstr "Uygulamadan çık"

#: data/shortcuts.ui:30
msgctxt "shortcut window"
msgid "Thumbnails"
msgstr "Küçük Resimler"

#: data/shortcuts.ui:34
msgctxt "shortcut window"
msgid "Open"
msgstr "Aç"

#: data/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Save As"
msgstr "Farklı Kaydet"

#: data/shortcuts.ui:46
msgctxt "shortcut window"
msgid "Move to Trash"
msgstr "Çöpe Taşı"

#: data/shortcuts.ui:52
msgctxt "shortcut window"
msgid "Delete"
msgstr "Sil"

#: libcheese/cheese-avatar-chooser.c:80 src/cheese-window.vala:263
#: src/cheese-window.vala:292 src/cheese-window.vala:367
msgid "_Cancel"
msgstr "_İptal"

#: libcheese/cheese-avatar-chooser.c:82
msgid "_Select"
msgstr "_Seç"

#: libcheese/cheese-avatar-widget.c:128 src/cheese-window.vala:691
msgid "Shutter sound"
msgstr "Deklanşör sesi"

#: libcheese/cheese-avatar-widget.c:270
msgid "_Take Another Picture"
msgstr "Başka Resim _Çek"

#: libcheese/cheese-camera.c:424 libcheese/cheese-camera.c:1604
msgid "One or more needed GStreamer elements are missing: "
msgstr "Gereken GStreamer ögelerinden bir veya daha çoğu eksik: "

#: libcheese/cheese-camera.c:1546
#, c-format
msgid "No device found"
msgstr "Aygıt bulunamadı"

#. Translators: This is a time format, like "09:05:02" for 9
#. * hours, 5 minutes, and 2 seconds. You may change ":" to
#. * the separator that your locale uses or use "%Id" instead
#. * of "%d" if your locale uses localized digits.
#.
#: libcheese/cheese-camera.c:1839
#, c-format
msgctxt "time format"
msgid "%02i:%02i:%02i"
msgstr "%02i:%02i:%02i"

#: libcheese/cheese-camera-device.c:552
msgid "Device capabilities not supported"
msgstr "Aygıt yetenekleri desteklenmedi"

#: libcheese/cheese-camera-device.c:685
msgid "Unknown device"
msgstr "Bilinmeyen aygıt"

#: libcheese/cheese-camera-device.c:704
msgid "Cancellable initialization not supported"
msgstr "İptal edilebilir ilklendirme desteklenmiyor"

#: src/cheese-application.vala:56
msgid "Start in wide mode"
msgstr "Geniş kipte başlat"

#: src/cheese-application.vala:59
msgid "Device to use as a camera"
msgstr "Kamera olarak kullanılacak aygıt"

#: src/cheese-application.vala:59
msgid "DEVICE"
msgstr "AYGIT"

#: src/cheese-application.vala:61
msgid "Output version information and exit"
msgstr "Sürüm bilgisini yazdır ve çık"

#: src/cheese-application.vala:63
msgid "Start in fullscreen mode"
msgstr "Tam ekran kipinde başlat"

#: src/cheese-application.vala:313
msgid "Webcam in use"
msgstr "Kullanılan web kamerası"

#: src/cheese-application.vala:575
msgid "translator-credits"
msgstr ""
"Barış Çiçek <baris@teamforce.name.tr>\n"
"Muhammet Kara <muhammetk@gmail.com>"

#: src/cheese-application.vala:577
msgid "Cheese Website"
msgstr "Peynir Web Sitesi"

#. Translators: a description of an effect (to be applied to images
#. *              from the webcam) which does nothing.
#: src/cheese-effects-manager.vala:51
msgid "No Effect"
msgstr "Etki Yok"

#: src/cheese-window.vala:235
#, c-format
msgid "Could not open %s"
msgstr "%s açılamadı"

#: src/cheese-window.vala:260
#, c-format
msgid "Are you sure you want to permanently delete the file?"
msgid_plural "Are you sure you want to permanently delete %d files?"
msgstr[0] "%d dosyayı kalıcı olarak silmek istediğinizden emin misiniz?"

#: src/cheese-window.vala:264
msgid "_Delete"
msgstr "_Sil"

#: src/cheese-window.vala:266
msgid "If you delete an item, it will be permanently lost"
msgid_plural "If you delete the items, they will be permanently lost"
msgstr[0] "Eğer bu ögeleri silerseniz, kalıcı olarak kaybolacaklar"

#: src/cheese-window.vala:290
#, c-format
msgid "Could not delete %s"
msgstr "%s silinemedi"

#: src/cheese-window.vala:293
msgid "Skip"
msgstr "Atla"

#: src/cheese-window.vala:294
msgid "Skip all"
msgstr "Tümünü atla"

#: src/cheese-window.vala:339
#, c-format
msgid "Could not move %s to trash"
msgstr "%s, çöpe taşınamadı"

#. Nothing selected.
#: src/cheese-window.vala:364
msgid "Save File"
msgstr "Dosyayı Kaydet"

#: src/cheese-window.vala:368
msgid "Save"
msgstr "Kaydet"

#: src/cheese-window.vala:398
#, c-format
msgid "Could not save %s"
msgstr "%s kaydedilemedi"

#: src/cheese-window.vala:819
msgid "Stop recording"
msgstr "Kaydı durdur"

#: src/cheese-window.vala:834
msgid "Record a video"
msgstr "Video kaydet"

#. FIXME: Set the effects action to be inactive.
#: src/cheese-window.vala:869
msgid "Stop taking pictures"
msgstr "Resim çekmeyi durdur"

#: src/cheese-window.vala:892
msgid "Take multiple photos"
msgstr "Çoklu fotoğraf çek"

#: src/cheese-window.vala:1086
msgid "No effects found"
msgstr "Etki yok"

#: src/cheese-window.vala:1210
msgid "There was an error playing video from the webcam"
msgstr "Web kamerasından video oynatırken hata oluştu"

#: src/cheese-window.vala:1419
msgid "Record a video using a webcam"
msgstr "Web kamerası kullanarak video kaydet"

#: src/cheese-window.vala:1425
msgid "Take multiple photos using a webcam"
msgstr "Web kamerası kullanarak çoklu fotoğraf çek"

#: src/cheese-window.vala:1437
msgid "Choose an Effect"
msgstr "Etki Seç"

#: src/cheese-window.vala:1448
msgid "Record a Video"
msgstr "Video Kaydet"

#: src/cheese-window.vala:1452
msgid "Take Multiple Photos"
msgstr "Çoklu Fotoğraf Çek"

#~ msgid "Cheese Webcam Booth"
#~ msgstr "Peynir Web Kamerası Standı"

#~ msgid "_About"
#~ msgstr "_Hakkında"

#~ msgid "_Quit"
#~ msgstr "_Çık"

#~ msgid "Failed to initialize device %s for capability probing"
#~ msgstr "Yetenek sınaması için %s aygıtı başlatılamadı"

#~ msgid "Share…"
#~ msgstr "Paylaş…"

#~ msgid "Move _All to Trash"
#~ msgstr "_Hepsini Çöpe Taşı"

#~ msgid "_Wide Mode"
#~ msgstr "_Geniş Kip"

#~ msgid "P_revious Effects"
#~ msgstr "Önceki _Etkiler"

#~ msgid "Ne_xt Effects"
#~ msgstr "_Sonraki Etkiler"

#~ msgid "_Take a Photo"
#~ msgstr "_Fotoğraf Çek"

#~ msgid "_Leave Fullscreen"
#~ msgstr "_Tam Ekrandan Çık"

#~ msgid "Picture hue"
#~ msgstr "Resim tonu"

#~ msgid "Whether to start in wide mode"
#~ msgstr "Geniş kipte başlatılsın mı"

#~ msgid ""
#~ "If set to true, Cheese will start up in wide mode, with the image "
#~ "collection placed on the right-hand side. Useful with small screens."
#~ msgstr ""
#~ "Eğer doğru (true) olarak ayarlanırsa, Cheese resim koleksiyonu sağ kenara "
#~ "yerleştirilecek biçimde geniş kipte başlatılacak. Küçük ekranlar için "
#~ "kullanışlıdır."

#~ msgid "Whether to start in fullscreen"
#~ msgstr "Tam ekran olarak başlatılsın mı"

#~ msgid "If set to true, Cheese will start up in fullscreen mode."
#~ msgstr ""
#~ "Eğer doğru (true) olarak ayarlanırsa, Cheese tam ekran kipinde başlar."

#~ msgid "_Discard photo"
#~ msgstr "_Fotoğrafı kaydetme"

#~ msgid "_Shoot"
#~ msgstr "_Çek"

#~ msgid "Mode:"
#~ msgstr "Kip:"

#~ msgid "- Take photos and videos from your webcam"
#~ msgstr "- Web kameranızdan fotoğraf ve video çekin"

#~ msgid ""
#~ "Run '%s --help' to see a full list of available command line options.\n"
#~ msgstr ""
#~ "Bütün  yardım fonksiyonları listesi için '%s --help' komutunu girin.\n"

#~ msgid "Another instance of Cheese is currently running\n"
#~ msgstr "Cheese'in başka bir kopyası çalışıyor.\n"

#~ msgid "Stop _Recording"
#~ msgstr "_Kaydı Durdur"

#~ msgid "Take _Multiple Photos"
#~ msgstr "_Çoklu Fotoğraf Çek"

#~ msgid "Previous"
#~ msgstr "Geri"

#~ msgid "Next"
#~ msgstr "İleri"

#~ msgid "Leave fullscreen"
#~ msgstr "Tam ekrandan çık"

#~ msgid "_Take a photo"
#~ msgstr "_Fotoğraf çek"

#~ msgid "_Contents"
#~ msgstr "İçi_ndekiler"

#~ msgid "_Edit"
#~ msgstr "_Düzenle"

#~ msgid "<b>Shutter</b>"
#~ msgstr "<b>Deklanşör</b>"

#~ msgid "Image properties"
#~ msgstr "Resim özellikleri"

#~ msgid "Photo width resolution"
#~ msgstr "Fotoğraf genişlik çözünürlüğü"

#~ msgid "Video width resolution"
#~ msgstr "Video genişlik çözünürlüğü"

#~ msgid ""
#~ "This program is free software; you can redistribute it and/or modify it "
#~ "under the terms of the GNU General Public License as published by the "
#~ "Free Software Foundation; either version 2 of the License, or (at your "
#~ "option) any later version.\n"
#~ "\n"
#~ "This program is distributed in the hope that it will be useful, but "
#~ "WITHOUT ANY WARRANTY; without even the implied warranty of "
#~ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General "
#~ "Public License for more details.\n"
#~ "\n"
#~ "You should have received a copy of the GNU General Public License along "
#~ "with this program. If not, see http://www.gnu.org/licenses/\n"
#~ msgstr ""
#~ "Bu program bir özgür yazılımdır ve Özgür Yazılım Vakfı tarafından "
#~ "yayınlanan GNU Genel Kamu Lisansı sürüm 2 ya da (tercihen) daha sonraki "
#~ "sürümleri altında tekrar dağıtabilir ya da değiştirebilirsiniz.\n"
#~ "\n"
#~ "Bu program faydalı olması amacıyla dağıtılmaktadır, ancak HİÇBİR "
#~ "GARANTİSİ YOKTUR; SATILABİLİRLİK ya da BİR AMACA UYGUNLUK gibi "
#~ "uygulanabilir garantisi de bulunmamaktadır. Ayrıntılar için GNU Genel "
#~ "Kamu Lisansı'nı inceleyiniz.\n"
#~ "\n"
#~ "Bu programla birlikte GNU Genel Kamu Lisansı'nın bir kopyasını almış "
#~ "olmalısınız. Eğer almadıysanız http://www.gnu.org/licenses/ adresine "
#~ "bakınız.\n"

#~ msgid ""
#~ "A list of effects applied on startup. Possible values are: \"mauve\", "
#~ "\"noir_blanc\", \"saturation\", \"hulk\", \"vertical-flip\", \"horizontal-"
#~ "flip\", \"shagadelic\", \"vertigo\", \"edge\", \"dice\" and \"warp\""
#~ msgstr ""
#~ "Başlangıçta uygulanacak etkiler. Geçerli değerler: \"mauve\", \"noir_blanc"
#~ "\", \"saturation\", \"hulk\", \"vertical-flip\", \"horizontal-flip\", "
#~ "\"shagadelic\", \"vertigo\", \"edge\", \"dice\" ve \"warp\""

#~ msgid ""
#~ "If set to true, then Cheese will have a feature allowing you to delete a "
#~ "file immediately and in-place, instead of moving it to the trash. This "
#~ "feature can be dangerous, so use caution."
#~ msgstr ""
#~ "Eğer seçiliyse, Peynir bir dosyayı çöpe taşımak yerine anında ve yerinde "
#~ "silmeye olanak sağlayacak bir özellik kullanır. Bu özellik tehlikeli "
#~ "olabilir o yüzden dikkatli kullanın."

#~ msgid "Selected Effects"
#~ msgstr "Seçili Etkiler"

#~ msgid "Whether to enable immediate deletion"
#~ msgstr "Anında silmenin etkinleştirilmesi"

#~ msgid "Switch to Burst Mode"
#~ msgstr "Şipşak Kipine Geç"

#~ msgid "Switch to Photo Mode"
#~ msgstr "Fotoğraf Kipine Geç"

#~ msgid "Switch to Video Mode"
#~ msgstr "Video Kipine Geç"

#~ msgid "Switch to the Effects Selector"
#~ msgstr "Etki Seçiciye Geç"

#~ msgid "Resolution"
#~ msgstr "<b>Çözünürlük</b>"

#~ msgid "Mauve"
#~ msgstr "Ebegümeci"

#~ msgid "Noir/Blanc"
#~ msgstr "Kara"

#~ msgid "Hulk"
#~ msgstr "Mauve"

#~ msgid "Vertical Flip"
#~ msgstr "Dikey Döndürme"

#~ msgid "Horizontal Flip"
#~ msgstr "Yatay Döndürme"

#~ msgid "Shagadelic"
#~ msgstr "Shagadelic"

#~ msgid "Vertigo"
#~ msgstr "Vertigo"

#~ msgid "Edge"
#~ msgstr "Kenar"

#~ msgid "Dice"
#~ msgstr "Zar"

#~ msgid "Warp"
#~ msgstr "Eğri Büğrü"

#~ msgid "%d "
#~ msgstr "%d "

#~ msgid "Please refer to the help for further information."
#~ msgstr "Lütfen daha fazla bilgi için yardıma başvurun."

#~ msgid "Delete _All"
#~ msgstr "Hepsini _Sil"

#~ msgid ""
#~ "Failed to launch program to show:\n"
#~ "%s\n"
#~ "%s"
#~ msgstr ""
#~ "Aşağıdakini göstermek için program başlatılırken başarısız olundu:\n"
#~ "%s\n"
#~ "%s"

#~ msgid "Error while deleting"
#~ msgstr "Silerken hata"

#~ msgid "The file \"%s\" cannot be deleted. Details: %s"
#~ msgstr "Dosya \"%s\" silinemiyor. Ayrıntılar: %s"

#~ msgid "Are you sure you want to permanently delete the %'d selected item?"
#~ msgid_plural ""
#~ "Are you sure you want to permanently delete the %'d selected items?"
#~ msgstr[0] ""
#~ "%'d seçili öğeyi kalıcı olarak silmek istediğinizden emin misiniz?"

#~ msgid "Unknown Error"
#~ msgstr "Bilinmeyen Hata"

#~ msgid "Cannot move file to trash, do you want to delete immediately?"
#~ msgstr "Dosya çöpe taşınamıyor, anında silmek ister misiniz?"

#~ msgid "The file \"%s\" cannot be moved to the trash. Details: %s"
#~ msgstr "Dosya \"%s\" çöpe taşınamıyor. Ayrıntılar: %s"

#~ msgid "Really move all photos and videos to the trash?"
#~ msgstr "Gerçekten tüm fotoğraflar ve videolar çöpe taşınsın mı?"

#~ msgid "Could not set the Account Photo"
#~ msgstr "Hesap Fotoğrafı atanamadı"

#~ msgid "Media files"
#~ msgstr "Ortam dosyaları"

#~ msgid "Unable to open help file for Cheese"
#~ msgstr "Peynir için yardım dosyası açılamadı"

#~ msgid ""
#~ "This program is free software; you can redistribute it and/or modify it "
#~ "under the terms of the GNU General Public License as published by the "
#~ "Free Software Foundation; either version 2 of the License, or (at your "
#~ "option) any later version.\n"
#~ msgstr ""
#~ "Bu program bir özgür yazılımdır onu Özgür Yazılım Vakfı tarafından "
#~ "yayınlanan GNU Genel Kamu Lisansı sürüm 2 ya da (tercihen) daha sonraki "
#~ "sürümleri altında tekrar dağıtabilir ya da değiştirebilirsiniz\n"

#~ msgid ""
#~ "This program is distributed in the hope that it will be useful, but "
#~ "WITHOUT ANY WARRANTY; without even the implied warranty of "
#~ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General "
#~ "Public License for more details.\n"
#~ msgstr ""
#~ "Bu program faydalı olması amacıyla dağıtılmaktadır, ancak HİÇBİR "
#~ "GARANTİSİ YOKTUR; SATILABİLİRLİK ya da BİR AMACA UYGUNLUK gibi "
#~ "uygulanabilir garantisi de bulunmamaktadır.  Daha fazla ayrıntı için GNU "
#~ "Genel Kamu Lisansı'na bakın.\n"

#~ msgid ""
#~ "You should have received a copy of the GNU General Public License along "
#~ "with this program. If not, see <http://www.gnu.org/licenses/>."
#~ msgstr ""
#~ "GNU Genel Kamu Lisansı'nın bir kopyasını bu program ile almış "
#~ "olmalısınız. Eğer almadıysanız <http://www.gnu.org/licenses/> adresine "
#~ "bakın."

#~ msgid "_Start Recording"
#~ msgstr "_Kayda Başla"

#~ msgid "_Set As Account Photo"
#~ msgstr "_Hesap Fotoğrafı Olarak Ata"

#~ msgid "Send by _Mail"
#~ msgstr "_Mektup ile Gönder"

#~ msgid "Send _To"
#~ msgstr "_Gönder"

#~ msgid "Export to F-_Spot"
#~ msgstr "F-_Spot'a Aktar"

#~ msgid "Export to _Flickr"
#~ msgstr "_Flickr'a Aktar"

#~ msgid "_Start recording"
#~ msgstr "_Kayda başla"

#~ msgid "Check your gstreamer installation"
#~ msgstr "Gstreamer kurulumunu kontrol edin"

#~ msgid "Be verbose"
#~ msgstr "Detaylı ol"

#~ msgid "Enable wide mode"
#~ msgstr "Geniş kipi etkinleştir"

#~ msgid ""
#~ "- Take photos and videos with your webcam, with fun graphical effects"
#~ msgstr ""
#~ "- Web kameranızdan fotoğraflar ve videolar çeker ve grafiksel etkiler "
#~ "uygular"

#~ msgid "Help"
#~ msgstr "Yardım"

#~ msgid ""
#~ "Failed to open browser to show:\n"
#~ "%s"
#~ msgstr ""
#~ "Aşağıdaki adresi göstermek için tarayıcı açılırken başarısız olundu:\n"
#~ "%s"

#~ msgid ""
#~ "Failed to open email client to send message to:\n"
#~ "%s"
#~ msgstr ""
#~ "Aşağıdaki iletiyi göndermek için e-posta istemcisi açılırken başarısız "
#~ "olundu:\n"
#~ "%s"

#~ msgid "_Back"
#~ msgstr "_Geri"
